import string
from random import choice


class StringUtils(object):
    CHARS_SET = string.ascii_letters + string.digits

    @staticmethod
    def random_string(length, chars=CHARS_SET):
        return "I-" + "".join(choice(chars) for _ in range(length))
