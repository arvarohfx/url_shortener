from django.views import View
from django.http import JsonResponse
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.base import ContextMixin
from django.shortcuts import render, redirect

from url_shortener.models import UrlQuery
from url_shortener.forms import UrlForm


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    template_name = ""

    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )

    @staticmethod
    def get_data(context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        return context


class IndexView(View, TemplateResponseMixin, JSONResponseMixin, ContextMixin):
    template_name = 'main/index.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        form = UrlForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data.get('url')
            url_obj = UrlQuery.create_url(url=url)
            return self.render_to_json_response({'success': True,
                                                 'short_url': request.build_absolute_uri() + url_obj.short_url})
        return self.render_to_json_response({'success': False,
                                             'errors': [(k, v[0]) for k, v in form.errors.items()]})


class RedirectView(View):
    template_name = 'main/error.html'

    def get(self, request, *args, **kwargs):
        url = UrlQuery.get_full_url(kwargs.get('short_url'))
        if url:
            return redirect(url.url)
        return render(request, self.template_name, context={"error": "Ссылка устарела"})
