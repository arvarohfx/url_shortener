from datetime import timedelta

from django.db import models
from django.utils import timezone

from url_shortener.models import TimestampedAbstractModel
from url_shortener.utils import StringUtils


def day_gap():
    return timezone.now() + timedelta(hours=24)


class Url(TimestampedAbstractModel):
    objects = models.Manager()

    url = models.TextField("Ccылка", blank=False, null=False)
    short_url = models.CharField("Короткая ссылка", blank=False, null=False, max_length=128)
    expired = models.DateTimeField("Время истечения срока действия", blank=True, null=True, default=day_gap)

    def __str__(self):
        return self.short_url

    class Meta:
        verbose_name = "Ссылка"
        verbose_name_plural = "Ссылки"


class UrlQuery(object):
    @staticmethod
    def get_full_url(short_url):
        return Url.objects.filter(short_url=short_url, expired__gt=timezone.now()).first()

    @staticmethod
    def create_url(url):
        """
        Если ссылка уже существует -> обновить время жизни и вернуть.
        Если короткая ссылка уже существует, выполнить ещё попытку.
        :param url:
        :return:
        """
        url_obj = Url.objects.filter(url=url).first()
        if url_obj:
            url_obj.expired = day_gap()
            return url_obj
        short_url = StringUtils.random_string(10)
        i = 1
        while i < 10:
            if not Url.objects.filter(short_url=short_url, expired__gt=timezone.now()).exists():
                url_obj = Url(url=url, short_url=short_url)
                url_obj.save()
                return url_obj
            i += 1
