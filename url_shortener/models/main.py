from django.db import models


class TimestampedAbstractModel(models.Model):
    """Абстрактная модель, содержащая временные метки"""
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)
    modified = models.DateTimeField(u'Дата модификации', auto_now=True)

    class Meta:
        abstract = True
        ordering = ("-created",)
