import requests
from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


class UrlForm(forms.Form):
    url = forms.CharField(error_messages={'required': 'Обязательное поле.'})

    fields = ['email', 'password']

    def __init__(self, *args, **kwargs):
        super(UrlForm, self).__init__(*args, **kwargs)

    def clean_url(self):
        url = self.cleaned_data.get('url').lower()
        if 'http' not in url:
            url = 'http://' + url
        validator = URLValidator()
        try:
            validator(url)
            requests.get(url)
        except ValidationError:
            raise ValidationError("Некорректная ссылка.")
        except requests.exceptions.ConnectionError:
            raise ValidationError("Адрес не существует.")
        if not url:
            raise ValidationError("Необходимо ввести ссылку.")
        if len(url) < 50:
            raise ValidationError("Ссылка уже достаточно короткая!")
        return url
