from django.conf.urls import url
from django.contrib import admin

from url_shortener.views import IndexView, RedirectView

app_name = 'personal'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'(?P<short_url>(I-)[A-Za-z0-9]{10})/$', RedirectView.as_view(), name='redirect'),

    url('admin/', admin.site.urls),
]
