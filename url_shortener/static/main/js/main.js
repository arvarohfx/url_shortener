$(function () {
    function sendFormAjax(frm) {
        frm.find("button").prop('disabled', true);
        $.ajax({
            url: frm.attr('action'),
            type: frm.attr('method'),
            data: frm.serialize(),
            success: function(data, textStatus, xhr) {
                frm.find("p.error").empty().hide();
                frm.find("button").prop('disabled', false);
                if (data.success) {
                    if (data.redirect_to) {
                        window.location.href = data.redirect_to;
                    } else if (data.short_url) {
                        $('.short-url').slideDown(100);
                        $('.result').html(data.short_url);
                        $('.result').show().effect('pulsate', 400);
                    }
                } else {
                    $.each(data.errors, function(index, element) {
                        err = $("#" + element[0]).nextAll(".error").first();
                        if (err.css("visibility") == "hidden") {
                            err.text(element[1]).slideDown(200);
                        } else {
                            err.text(element[1]).show().effect('bounce', {times:3}, {distance: 2}, 1600);
                        }

                    });
                }
            }
        });
    }

    $("form").submit(function(e) {
        e.preventDefault();
        sendFormAjax($(this));
    });

    $(".result").on('click', function(){
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });

    $(".result").hover(function() {
        $('.tool-tip').slideDown(200);
    }, function() {
        $('.tool-tip').slideUp(200);
    });
});
