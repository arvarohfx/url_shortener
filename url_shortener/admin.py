from django.contrib import admin
from url_shortener.models import Url


class UrlAdmin(admin.ModelAdmin):
    list_display = ('url', 'short_url')
    ordering = ('-created',)


admin.site.register(Url, UrlAdmin)
